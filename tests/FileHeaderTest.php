<?php
/**
 * XLIFF file header test
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */

use PHPUnit\Framework\TestCase;

use ArteQ\CSX\Xliff\FileHeader;

class FileHeaderTest extends TestCase
{
	public function testCanReadFileHeader()
	{
		$dom = new \DOMDocument();
		$dom->load(__DIR__.'/test.mqxliff', \LIBXML_NOWARNING);

		$fileHeader = new FileHeader();
		$fileHeader->read($dom);
		$fileHeaderAttributes = $fileHeader->getAttributes();

		$this->assertEquals('RB_16_2018.doc', $fileHeaderAttributes['original']);
		$this->assertEquals('64ebf1c8-6d16-4e4a-b258-8fd7f7b075ae', $fileHeaderAttributes['mq:id']);
		$this->assertEquals('79aa68a7-8a53-e811-add8-8f0135b6607c', $fileHeaderAttributes['mq:projectid']);
		$this->assertEquals('pl', $fileHeaderAttributes['source-language']);
		$this->assertEquals('en-gb', $fileHeaderAttributes['target-language']);
		$this->assertEquals('x-rtf', $fileHeaderAttributes['datatype']);
	}

	public function testCanWriteFileHeader()
	{
		// create empty document
		$dom = new \DOMDocument('1.0', 'UTF-8');
		$xliff = $dom->createElement("xliff");
		$dom->appendChild($xliff);

		$fileHeader = new FileHeader();
		$fileHeader->addAttribute('attribute1', 'value1');
		$fileHeader->addAttribute('attribute2', 'value2');

		$xliff->appendChild($fileHeader->write($dom));

		$xpath = new \DOMXPath($dom);

		$this->assertEquals(2, $xpath->evaluate('//xliff/file')[0]->attributes->length);

		$this->assertNotEmpty($xpath->evaluate('//xliff/file[@attribute1="value1"]')->item(0));
		$this->assertNotEmpty($xpath->evaluate('//xliff/file[@attribute2="value2"]')->item(0));
		$this->assertEmpty($xpath->evaluate('//xliff/file[@attribute3="value3"]')->item(0));

		$fileHeaderAttributes = $fileHeader->getAttributes();
		$this->assertEquals('value1', $fileHeaderAttributes['attribute1']);
		$this->assertEquals('value2', $fileHeaderAttributes['attribute2']);
	}

	public function testCanReadElements()
	{
		$dom = new \DOMDocument();
		$dom->load(__DIR__.'/test.mqxliff', \LIBXML_NOWARNING);

		$fileHeader = new FileHeader();
		$fileHeader->read($dom);
		$headerElements = $fileHeader->getElements();

		$this->assertCount(3, $headerElements);

		$this->assertEquals('tool', $headerElements[0]['name']);
		$this->assertCount(4, $headerElements[0]['attributes']);
		$this->assertArrayHasKey('tool-id', $headerElements[0]['attributes']);
		$this->assertArrayHasKey('tool-name', $headerElements[0]['attributes']);
		$this->assertArrayHasKey('tool-version', $headerElements[0]['attributes']);
		$this->assertArrayHasKey('tool-company', $headerElements[0]['attributes']);

		$this->assertEquals('mq:export-path', $headerElements[1]['name']);
		$this->assertEquals('mq:docinformation', $headerElements[2]['name']);
	}

	public function testCanWriteElements()
	{
		// create empty document
		$dom = new \DOMDocument('1.0', 'UTF-8');
		$xliff = $dom->createElement("xliff");
		$dom->appendChild($xliff);

		$fileHeader = new FileHeader();
		$fileHeader->addElement('nodeWithoutValue');
		$fileHeader->addElement('nodeWithValue', 'value');
		$fileHeader->addElement('nodeWithValueAndAttributes', 'value', ['attr1' => 'value1', 'attr2' => 'value2']);
		$fileHeader->addElement('nodeWithoutValueAndAttributes', null, ['attr1' => 'value1', 'attr2' => 'value2']);

		$xliff->appendChild($fileHeader->write($dom));

		$xpath = new \DOMXPath($dom);

		$this->assertEquals(4, $xpath->evaluate('//xliff/file/header/*')->length);
		$this->assertNotEmpty($xpath->evaluate('//xliff/file/header/nodeWithoutValue')[0]);
		$this->assertNotEmpty($xpath->evaluate('//xliff/file/header/nodeWithValue')[0]);
		$this->assertEquals('value', $xpath->evaluate('//xliff/file/header/nodeWithValue')[0]->nodeValue);
		$this->assertEquals('value', $xpath->evaluate('//xliff/file/header/nodeWithValueAndAttributes')[0]->nodeValue);
		$this->assertEquals('value1', $xpath->evaluate('//xliff/file/header/nodeWithValueAndAttributes/@attr1')[0]->nodeValue);
		$this->assertEquals('value2', $xpath->evaluate('//xliff/file/header/nodeWithValueAndAttributes/@attr2')[0]->nodeValue);
		$this->assertEmpty($xpath->evaluate('//xliff/file/header/nodeWithValueAndAttributes/@fake')[0]);
	}

	public function testCanWriteChildElements()
	{
		// create empty document
		$dom = new \DOMDocument('1.0', 'UTF-8');
		$xliff = $dom->createElement("xliff");
		$dom->appendChild($xliff);

		$sub1 = ['name' => 'subName1', 'value' => 'subValue1', 'attributes' => ['x' => 'y']];
		$sub2 = ['name' => 'subName2', 'value' => 'subValue2', 'attributes' => ['y' => 'x']];

		$fileHeader = new FileHeader();
		$fileHeader->addElement('node', null, null, [$sub1, $sub2]);
		$xliff->appendChild($fileHeader->write($dom));

		$xpath = new \DOMXPath($dom);

		$this->assertEquals('subValue1', $xpath->evaluate('//xliff/file/header/node/subName1')[0]->nodeValue);
		$this->assertEquals('y', $xpath->evaluate('//xliff/file/header/node/subName1/@x')[0]->nodeValue);
		$this->assertEquals('subValue2', $xpath->evaluate('//xliff/file/header/node/subName2')[0]->nodeValue);
		$this->assertEquals('x', $xpath->evaluate('//xliff/file/header/node/subName2/@y')[0]->nodeValue);
	}
}