<?php
/**
 * XLIFF writer test
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */

use PHPUnit\Framework\TestCase;

use ArteQ\CSX\Xliff\Xliff;
use ArteQ\CSX\Xliff\FileHeader;
use ArteQ\CSX\Xliff\TransUnit;

class WriterTest extends TestCase
{
	private $xliff;

	public function setUp()
	{
		$xliff = new Xliff();
		$this->xliff = $xliff;
	}

	public function testCanCreateEmpty()
	{
		$this->xliff->setFileHeader(new FileHeader());

		$xml = $this->xliff->saveXML();
		$this->assertContains('<xliff', $xml);
		$this->assertContains('<?xml', $xml);
		$this->assertContains('UTF-8', $xml);
		
		$this->assertContains('version', $xml);
		$this->assertContains('urn:oasis:names:tc:xliff:document:1.2', $xml);
	}

	public function testCanAddHeader()
	{
		$fileHeader = new FileHeader();
		$fileHeader->addAttribute('original', 'RB_16_2018.doc');
		$fileHeader->addAttribute('mq:id', '64ebf1c8-6d16-4e4a-b258-8fd7f7b075ae');
		$fileHeader->addAttribute('mq:projectid', '79aa68a7-8a53-e811-add8-8f0135b6607c');
		$fileHeader->addAttribute('source-language', 'pl');
		$fileHeader->addAttribute('target-language', 'en-gb');
		$fileHeader->addAttribute('datatype', 'x-rtf');

		$this->xliff->setFileHeader($fileHeader);

		$xml = $this->xliff->saveXml();
		$this->assertContains('<file', $xml);
		$this->assertContains('original="RB_16_2018.doc"', $xml);
		$this->assertContains('mq:id="64ebf1c8-6d16-4e4a-b258-8fd7f7b075ae"', $xml);
	}

	public function testCanCreateTransUnits()
	{
		$this->xliff->setFileHeader(new FileHeader());

		$tu1 = new TransUnit();
		$tu1->setSegment(['source' => 'siema', 'target' => 'no co tam?', 'attributes' => ['id' => 1]]);
		
		$tu2 = new TransUnit();
		$tu2->setSegment(['source' => 'aaa', 'target' => 'bbb', 'attributes' => ['id' => 2]]);


		$comments = [
			'mrk_123_start' => ['value' => 'komentarz', 'attributes' => ['id' => 1]]
		];

		$tu3 = new TransUnit();
		$tu3->setSegment(['source' => 'bbb', 'target' => 'ccc', 'attributes' => ['id' => 3], 'comments' => $comments]);

		$this->xliff->addTransUnit($tu1);
		$this->xliff->addTransUnit($tu2);
		$this->xliff->addTransUnit($tu3);

		$xml = $this->xliff->saveXML();
		$this->assertContains('<source>aaa</source>', $xml);
		$this->assertContains('<target>bbb</target>', $xml);
	}
}