<?php
/**
 * XLIFF reader test
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */

use PHPUnit\Framework\TestCase;

use ArteQ\CSX\Xliff\Xliff;

class ReaderTest extends TestCase
{
	private $xliff;

	public function setUp()
	{
		$xliff = new Xliff();
		$xliff->readFile(__DIR__.'/test.mqxliff');
		$this->xliff = $xliff;
	}

	public function testCanReadFileHeader()
	{
		$fileHeader = $this->xliff->getFileHeader();
		$fileHeaderAttributes = $fileHeader->getAttributes();

		$this->assertEquals('RB_16_2018.doc', $fileHeaderAttributes['original']);
		$this->assertEquals('64ebf1c8-6d16-4e4a-b258-8fd7f7b075ae', $fileHeaderAttributes['mq:id']);
		$this->assertEquals('79aa68a7-8a53-e811-add8-8f0135b6607c', $fileHeaderAttributes['mq:projectid']);
		$this->assertEquals('pl', $fileHeaderAttributes['source-language']);
		$this->assertEquals('en-gb', $fileHeaderAttributes['target-language']);
		$this->assertEquals('x-rtf', $fileHeaderAttributes['datatype']);
	}

	public function testCanReadTransUnits()
	{
		$transUnits = $this->xliff->getTransUnits();
		$this->assertCount(12, $transUnits);

		$segment = $transUnits[0]->getSegment();
		$this->assertEquals('Raport bieżący nr 16/2018 z dnia 09.05.2018', $segment['source']);
		$this->assertEquals('Current Report No. 16/2018 of May 9th 2018', $segment['target']);
		$this->assertCount(1, $segment['targetAttributes']);
		$this->assertCount(2, $segment['sourceAttributes']);
		$this->assertCount(18, $segment['attributes']);
		$this->assertCount(0, $segment['tags']);

		$segment = $transUnits[2]->getSegment();
		$this->assertCount(2, $segment['tags']);
		$this->assertEquals('❶Rekomendacja Zarządu w sprawie podziału zysku Spółki za rok 2017❷', $segment['source']);
		$this->assertEquals('Management Board’s recommendation on allocation of Grupa Azoty’s profit for 2017❷', $segment['target']);
	}

	public function testCanReadInvalidTransUnits()
	{
		$xliff = new Xliff();
		$xliff->readFile(__DIR__.'/test.mqxliff', false);
		$transUnits = $xliff->getTransUnits();
		$this->assertCount(13, $transUnits);
	}
}