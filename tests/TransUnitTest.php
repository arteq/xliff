<?php
/**
 * XLIFF trans unit test
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */

use PHPUnit\Framework\TestCase;

use ArteQ\CSX\Xliff\TransUnit;
use ArteQ\CSX\Xliff\Tag;
use ArteQ\CSX\Xliff\Xliff;

class TransUnitTest extends TestCase
{
	public function testCanCollapseTags()
	{
		$dom = new \DOMDocument();
		$dom->load(__DIR__.'/test.mqxliff', \LIBXML_NOWARNING);

		$xpath = new \DOMXPath($dom);
		$xpath->registerNamespace('xliff', 'urn:oasis:names:tc:xliff:document:1.2');
		$srcElement = $xpath->evaluate('//xliff:xliff//xliff:trans-unit[@id="3"]/xliff:source')[0];
		$trgElement = $xpath->evaluate('//xliff:xliff//xliff:trans-unit[@id="3"]/xliff:target')[0];

		$transUnit = new TransUnit();
		$transUnit->collapseEditMarks($srcElement);
		$transUnit->collapseEditMarks($trgElement);

		$txtSrc = $transUnit->collapseTags($srcElement);
		$txtTrg = $transUnit->collapseTags($trgElement);

		$this->assertContains('❶', $txtSrc);
		$this->assertContains('❷', $txtSrc);
		$this->assertContains('❷', $txtTrg);

		$this->assertNotContains('<bpt', $txtSrc);
		$this->assertNotContains('<ept', $txtSrc);
		
	}

	public function testCanExpendTags()
	{
		$dom = new \DOMDocument("1.0", "UTF-8");

		$tag1 = new Tag('bpt', '{}', ['id' => 1, 'type' => 'bold']);
		$tag1->createPlaceholder(0);

		$tag2 = new Tag('ept', '{xyz}', ['id' => 1]);
		$tag2->createPlaceholder(1);

		$tags = [
			'bpt_1' => $tag1,
			'ept_1' => $tag2,
		];

		$source = '❶Rekomendacja Zarządu w sprawie podziału zysku Spółki za rok 2017❷';

		$transUnit = new TransUnit();
		$transUnit->setSegment(['source' => $source, 'tags' => $tags]);

		$dom->appendChild($transUnit->write($dom));
		$xml = $dom->saveXML();

		$this->assertNotContains('❶', $xml);
		$this->assertNotContains('❷', $xml);
		$this->assertContains('<bpt id="1" type="bold">{}</bpt>', $xml);
		$this->assertContains('<ept id="1">{xyz}</ept>', $xml);
	}

	public function testCanCollapseEditMarks()
	{
		$dom = new \DOMDocument();
		$dom->load(__DIR__.'/mrk.mqxliff', \LIBXML_NOWARNING);

		$xpath = new \DOMXPath($dom);
		$xpath->registerNamespace('xliff', 'urn:oasis:names:tc:xliff:document:1.2');
		$srcElement = $xpath->evaluate('//xliff:xliff//xliff:trans-unit[@id="1"]/xliff:source')[0];
		$trgElement = $xpath->evaluate('//xliff:xliff//xliff:trans-unit[@id="1"]/xliff:target')[0];
		$srcExpectElement = $xpath->evaluate('//xliff:xliff//xliff:trans-unit[@id="1"]/xliff:sourceExpected')[0];

		$transUnit = new TransUnit();
		$transUnit->collapseEditMarks($srcElement);
		$transUnit->collapseEditMarks($trgElement);

		$txtSrc = $transUnit->collapseTags($srcElement);
		$txtTrg = $transUnit->collapseTags($trgElement);

		$this->assertEquals($txtSrc, $srcExpectElement->nodeValue);
	}

	public function testCanReadTransUnitComments()
	{
		$xliff = new Xliff();
		$xliff->readFile(__DIR__.'/mrk.mqxliff');

		$transUnits = $xliff->getTransUnits();
		$segment1 = $transUnits[0]->getSegment();
		$segment2 = $transUnits[1]->getSegment();
		$segment3 = $transUnits[2]->getSegment();

		$this->assertCount(1, $segment1['comments']);
		$this->assertEquals('to chyba częśc tej samej frazy??', $segment1['comments'][0]['value']);
		$this->assertCount(7, $segment1['comments'][0]['attributes']);

		$this->assertCount(1, $segment2['comments']);
		$this->assertCount(0, $segment3['comments']);
	}

	public function testCanCollapseXTags()
	{
		$dom = new \DOMDocument();
		$dom->load(__DIR__.'/test.mqxliff', \LIBXML_NOWARNING);

		$xpath = new \DOMXPath($dom);
		$xpath->registerNamespace('xliff', 'urn:oasis:names:tc:xliff:document:1.2');
		$srcElement = $xpath->evaluate('//xliff:xliff//xliff:trans-unit[@id="4"]/xliff:source')[0];

		$transUnit = new TransUnit();
		$transUnit->collapseEditMarks($srcElement);

		$txtSrc = $transUnit->collapseTags($srcElement);

		$this->assertContains('❶', $txtSrc);

		$this->assertNotContains('<x', $txtSrc);
	}

	public function testCantOverflowTagCollapse()
	{
		$dom = new \DOMDocument();
		$dom->load(__DIR__.'/test.mqxliff', \LIBXML_NOWARNING);

		$xpath = new \DOMXPath($dom);
		$xpath->registerNamespace('xliff', 'urn:oasis:names:tc:xliff:document:1.2');
		$srcElement = $xpath->evaluate('//xliff:xliff//xliff:trans-unit[@id="tagsOverflow"]/xliff:source')[0];

		$transUnit = new TransUnit();
		$transUnit->collapseEditMarks($srcElement);

		$txtSrc = $transUnit->collapseTags($srcElement);

		$this->assertContains('❶', $txtSrc);
		$this->assertContains('❿', $txtSrc);
		$this->assertContains('①', $txtSrc);
		$this->assertContains('⑩', $txtSrc);
		$this->assertContains('⑳', $txtSrc);
		$this->assertContains('Ⓐ', $txtSrc);
		$this->assertContains('Ⓕ', $txtSrc);
		$this->assertContains('<ph id="37">', $txtSrc);
	}

	public function testCanGetValidStatus()
	{
		$dom = new \DOMDocument();
		$dom->load(__DIR__.'/test.mqxliff', \LIBXML_NOWARNING);

		$xpath = new \DOMXPath($dom);
		$xpath->registerNamespace('xliff', 'urn:oasis:names:tc:xliff:document:1.2');
		$srcElement = $xpath->evaluate('//xliff:xliff//xliff:trans-unit[@id="1"]/xliff:source')[0];

		$transUnit = new TransUnit();
		$transUnit->collapseEditMarks($srcElement);

		$txtSrc = $transUnit->collapseTags($srcElement);

		$this->assertTrue($transUnit->isValid());
	}

	public function testCanGetInvalidStatus()
	{
		$dom = new \DOMDocument();
		$dom->load(__DIR__.'/test.mqxliff', \LIBXML_NOWARNING);

		$xpath = new \DOMXPath($dom);
		$xpath->registerNamespace('xliff', 'urn:oasis:names:tc:xliff:document:1.2');
		$srcElement = $xpath->evaluate('//xliff:xliff//xliff:trans-unit[@id="tagsOverflow"]/xliff:source')[0];

		$transUnit = new TransUnit();
		$transUnit->collapseEditMarks($srcElement);

		$txtSrc = $transUnit->collapseTags($srcElement);

		$this->assertFalse($transUnit->isValid());
	}
}