# memoQ XLIFF reader/writer

## Description
Helper library for quick read/write [memoQ](https://www.memoq.com/) generated [XLIFF](https://en.wikipedia.org/wiki/XLIFF) files. It provides easy access to plain text source/target translation unit segments, with flatten `<mrk>` post-edit marks (insert and delete) and replaces memoQ internal tags (`<bpt>`, `<ept>`, `<sub>`, `<it>`, `<ph>`, `<ut>`, `<hi>`) with one char UTF-8 symbols (black circled number 1-10, white circled number 1-20 or circled letter A-F) for simple edit in 3rd application. The tags placeholders are replaced back to oryginal XML tag elements uppon file save.

## Installation
Run composer to fetch project from bitbucket repository:
```sh
$ composer require arteq/xliff
```

Optionally you can run tests (phpUnit, CS-Fixer, PHPStan):
```sh
$ make
```

**Note**: code quality tests require PHP 7.x. If you run PHP 5.6 you can use simplified `composer_php56.json` file instead and only run phpUnit tests: 
```
$ env COMPOSER=composer_php56.json composer install
$ make phpunit
```

The library itself will run just fine on both PHP 5.x and 7.x.


## Usage

### Read file and show all content
```php
<?php
require __DIR__ . '/vendor/autoload.php';

use ArteQ\CSX\Xliff\Xliff;

try
{
	$xliff = new Xliff();
	$xliff->readFile('sample.mqxliff');

	$fileHeader = $xliff->getFileHeader();
	$transUnits = $xliff->getTransUnits();

	// show top level <file> attributes
	var_dump($fileHeader->getAttributes());

	// show <file/header> elements with attributes
	// note: some elements are ignored during read, see: src/FileHeader.php $ignoredHeaderElements
	var_dump($fileHeader->getElements());

	// show translation units with attributes, tags and plain-text source/target
	foreach ($transUnits as $transUnit)
	{
		var_dump($transUnit->getSegment());
	}
}
catch (\Exception $e)
{
	$error = $e->getMessage();
}

```

### Create new segment and write to file
```php
$tu = new TransUnit();
$tu->setSegment(['source' => 'Source segment.', 'target' => 'Translated segment.', 'attributes' => ['id' => 1]]);

$this->xliff->addTransUnit($tu);
$xliff->saveFile('file.mqxliff');
```

## Examples
For more code examples see **tests** directory.

For simple online parser tool see: https://test.asd123.pl/cs/xliff_debug/
