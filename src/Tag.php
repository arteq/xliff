<?php
/**
 * MemoQ inline tags
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */
namespace ArteQ\CSX\Xliff;

class Tag
{
	/**
	 * List of tag placeholders as UTF-8 chars:
	 * - circled numbers 1-10 (black)
	 * - circled numbers 1-20 (white)
	 * - circled letters A-F
	 * @var array
	 */ 
	const PLACEHOLDERS = [
		"\xE2\x9D\xB6",
		"\xE2\x9D\xB7",
		"\xE2\x9D\xB8",
		"\xE2\x9D\xB9",
		"\xE2\x9D\xBA",
		"\xE2\x9D\xBB",
		"\xE2\x9D\xBC",
		"\xE2\x9D\xBD",
		"\xE2\x9D\xBE",
		"\xE2\x9D\xBF",
		"\xE2\x91\xA0",
		"\xE2\x91\xA1",
		"\xE2\x91\xA2",
		"\xE2\x91\xA3",
		"\xE2\x91\xA4",
		"\xE2\x91\xA5",
		"\xE2\x91\xA6",
		"\xE2\x91\xA7",
		"\xE2\x91\xA8",
		"\xE2\x91\xA9",
		"\xE2\x91\xAA",
		"\xE2\x91\xAB",
		"\xE2\x91\xAC",
		"\xE2\x91\xAD",
		"\xE2\x91\xAE",
		"\xE2\x91\xAF",
		"\xE2\x91\xB0",
		"\xE2\x91\xB1",
		"\xE2\x91\xB2",
		"\xE2\x91\xB3",
		"\xE2\x92\xB6",
		"\xE2\x92\xB7",
		"\xE2\x92\xB8",
		"\xE2\x92\xB9",
		"\xE2\x92\xBA",
		"\xE2\x92\xBB",
	];

	/**
	 * Tag name
	 * @var string
	 */ 
	private $name;

	/**
	 * Tag inner content
	 * @var string
	 */ 
	private $value;

	/**
	 * Tag attributes
	 * @var array
	 */ 
	private $attributes;

	/**
	 * Tag one char placeholder as represented in plain text segment
	 * @var string 
	 */ 
	private $placeholder;

	/**
	 * Tag identifier: name_id
	 * @var string
	 */  
	private $hash;

	/**
	 * Create tag element, generate unique hash
	 * 
	 * @var string $name
	 * @var mixed $value
	 * @var array $attributes
	 */ 
	public function __construct($name, $value = null, $attributes = [])
	{
		$this->name = $name;
		$this->value = $value;

		foreach ($attributes as $attr_key => $attr_value)
		{
			$this->attributes[$attr_key] = $attr_value;
		}

		$this->generateHash();
	}

	/**
	 * Get tag name
	 * 
	 * @return string
	 */ 
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Get tag inner content
	 * 
	 * @return string
	 */ 
	public function getValue()
	{
		return $this->value;
	}
	
	/**
	 * Get tag attributes
	 * 
	 * @return array
	 */ 
	public function getAttributes()
	{
		return $this->attributes;
	}

	/**
	 * Set tag placeholder based on number of already created tags in segment
	 * 
	 * @param int $nr
	 */ 
	public function createPlaceholder($nr)
	{
		$placeholders = self::PLACEHOLDERS;
		if (!isset($placeholders[$nr]))
			throw new XliffException('Not enough placeholders for tag replace');

		$this->placeholder = $placeholders[$nr];
	}

	/**
	 * Set tag placeholder char
	 * 
	 * @param string $placeholder
	 */ 
	public function setPlaceholder($placeholder)
	{
		$this->placeholder = $placeholder;
	}

	/**
	 * Get tag placeholder char
	 * 
	 * @return string
	 */ 
	public function getPlaceholder()
	{
		return $this->placeholder;
	}

	/**
	 * Get tag hash
	 * 
	 * @return string
	 */ 
	public function getHash()
	{
		return $this->hash;
	}

	/**
	 * Return tag element ready to be written in segment
	 * 
	 * @param \DOMDocument $dom
	 * @return \DOMElement
	 */ 
	public function write(\DOMDocument $dom)
	{
		$tag = $dom->createElement($this->name, $this->value);
		foreach ($this->attributes as $name => $value)
		{
			$tag->setAttribute($name, $value);
		}

		return $tag;
	}

	/**
	 * Generate tag hash: name_id
	 * 
	 * @param string $key attribute key used for hash generation
	 */ 
	private function generateHash($key = 'id')
	{
		$hash = $this->name;

		if ($hash === 'mrk')
		{
			// generate hash for comments marks
			$hash .= '_';
			$hash .= $this->attributes['mq:ownerid'];
			$hash .= '_';
			$hash .= $this->attributes['mq:type'];
		}
		else
		{
			// generate hash for inline tags
			foreach ($this->attributes as $attr_key => $attr_value)
			{
				if ($attr_key === $key)
				{
					$hash .= '_' . $attr_value;

					break;
				}
			}
		}

		$this->hash = $hash;
	}
}
