<?php
/**
 * XLIFF 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */
namespace ArteQ\CSX\Xliff;

class Xliff
{
	const XML_VERSION = '1.0';
	const XML_ENCODING = 'UTF-8';
	const XLIFF_VERSION = '1.2';
	const XLIFF_NS = [
		'xliff' => 'urn:oasis:names:tc:xliff:document:1.2',
		'mq:attr' => 'MQXliff-BiLingRefDoc',
		'xsi:attr' => 'http://www.w3.org/2001/XMLSchema-instance',
	];

	/**
	 * XLIFF file header 
	 * @var FileHeader
	 */ 
	private $fileHeader;

	/**
	 * Translation units
	 * @var array
	 */ 
	private $transUnits = [];

	/**
	 * Read xliff file, parse header and translation units
	 * 
	 * @param string $path
	 * @param bool $includeOnlyValid
	 */ 
	public function readFile($path, $includeOnlyValid = true)
	{
		$dom = new \DOMDocument();
		if (!$dom->load($path, \LIBXML_NOWARNING | \LIBXML_NOERROR))
			throw new XliffException(sprintf("Can't create DOMDocument from file '%s'", $path));

		// read header
		if (empty($this->fileHeader))
			$this->fileHeader = new FileHeader();
		$this->fileHeader->read($dom);

		// read trans-units
		$xpath = new \DOMXPath($dom);
		$xpath->registerNamespace('xliff', self::XLIFF_NS['xliff']);
		foreach ($xpath->query('//xliff:trans-unit') as $transUnit)
		{
			$tu = new TransUnit();
			$tu->read($transUnit);

			if ($includeOnlyValid === true)
			{
				if ($tu->isValid() === true)
					$this->transUnits[] = $tu;
			}
			else
			{
				$this->transUnits[] = $tu;
			}
		}
	}

	/**
	 * Save generated xliff file to disk
	 * 
	 * @param string $path
	 */ 
	public function saveFile($path)
	{
		$xml = $this->saveXML();
		file_put_contents($path, $xml);
	}

	/**
	 * Generate xliff file and return as formated XML string
	 * 
	 * @return string
	 */ 
	public function saveXML()
	{
 		// create empty document
		$dom = new \DOMDocument(self::XML_VERSION, self::XML_ENCODING);
		$dom->formatOutput = true;

		// create xliff root element
		$xliff = $dom->createElement('xliff');

		// add version attribute
		$xliffVersion = $dom->createAttribute('version');
		$xliffVersion->value = self::XLIFF_VERSION;
		$xliff->appendChild($xliffVersion);

		$dom->appendChild($xliff);

		// add xliff namespaces
		foreach (self::XLIFF_NS as $nsName => $nsValue)
		{
			$attr = $dom->createAttributeNS($nsValue, $nsName);
		}

		// add file header
		$xliff->appendChild($this->fileHeader->write($dom));

		// create body for transUnits
		$body = $dom->createElement('body');
		foreach ($this->transUnits as $transUnit)
		{
			$body->appendChild($transUnit->write($dom));
		} 

		// get <file> element
		$xpath = new \DOMXPath($dom);
		$file = $xpath->evaluate('//xliff/file')->item(0);
		$file->appendChild($body);

		return $dom->saveXML();
	}

	/**
	 * Set file header
	 * 
	 * @param FileHeader $fileHeader
	 */ 
	public function setFileHeader(FileHeader $fileHeader)
	{
		$this->fileHeader = $fileHeader;
	}

	/**
	 * Get file header
	 * 
	 * @return FileHeader
	 */ 
	public function getFileHeader()
	{
		return $this->fileHeader;
	}

	/**
	 * Add single translation unit to list
	 * 
	 * @param TransUnit $transUnit
	 */ 
	public function addTransUnit(TransUnit $transUnit)
	{
		$this->transUnits[] = $transUnit;
	}

	/**
	 * Set all translation units in one call
	 * 
	 * @param array $transUnits
	 */ 
	public function setTransUnits($transUnits = [])
	{
		$this->transUnits = $transUnits;
	}

	/**
	 * Get array of translation units
	 * 
	 * @return array
	 */ 
	public function getTransUnits()
	{
		return $this->transUnits;	
	}
}