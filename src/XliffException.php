<?php
/**
 * Xliff exception
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */
 namespace ArteQ\CSX\Xliff;

 class XliffException extends \Exception
 {
 }