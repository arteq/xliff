<?php
/**
 * XLIFF trans-unit
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */
namespace ArteQ\CSX\Xliff;

class TransUnit
{
	const XLIFF_NS = 'urn:oasis:names:tc:xliff:document:1.2';

	/**
	 * List of possible inline tags for collapse/expand
	 * @var array
	 */ 
	const TAGS = ['bpt', 'ept', 'sub', 'it', 'ph', 'ut', 'hi', 'mrk', 'x'];

	/**
	 * List of top level <transUnit> attributes
	 * @var array
	 */ 
	protected $attributes = [];

	/**
	 * List of combined Tag for both source & target elements
	 * @var array
	 */ 
	protected $tags = [];

	/**
	 * Plain text source element representation
	 * @var string
	 */ 
	protected $source = '';

	/**
	 * Source element
	 * @var \DOMElement
	 */ 
	protected $sourceElement;

	/**
	 * Source element attributes
	 * @var array
	 */  
	protected $sourceAttributes = [];

	/**
	 * Plain text target element representation
	 * @var string
	 */ 
	protected $target = '';

	/**
	 * Target element
	 * @var \DOMElement
	 */ 
	protected $targetElement;

	/**
	 * Target element attributes
	 * @var array
	 */ 
	protected $targetAttributes = [];

	/**
	 * Segment comments
	 * @var array
	 */ 
	protected $comments = [];

	/**
	 * Segmnet marks (ex comments)
	 * @var array
	 */ 
	protected $marks = [];

	/**
	 * Is translation unit valid (ex all tags has been properly collapsed)
	 * @var bool
	 */ 
	private $isValid = true;

	/**
	 * Parse translation unit: save attributes, collapse edit marks and memoq internal tags
	 * 
	 * @param \DOMElement $transUnit
	 */ 
	public function read(\DOMElement $transUnit)
	{
		// read trans-unit attributes
		foreach ($transUnit->attributes as $fileAttribute)
		{
			$this->attributes[$fileAttribute->nodeName] = $fileAttribute->nodeValue;
		}

		// read source
		$xpath = new \DOMXPath($transUnit->ownerDocument);
		$xpath->registerNamespace('xliff', self::XLIFF_NS);
		$xpath->registerNamespace('mq', 'MQXliff');
		$this->sourceElement = $xpath->evaluate('xliff:source', $transUnit)->item(0);

		foreach ($this->sourceElement->attributes as $attr)
		{
			$this->sourceAttributes[$attr->nodeName] = $attr->nodeValue;
		}

		$this->collapseEditMarks($this->sourceElement);
		$this->source = $this->collapseTags($this->sourceElement);

		// read target
		$this->targetElement = $xpath->evaluate('xliff:target', $transUnit)->item(0);

		foreach ($this->targetElement->attributes as $attr)
		{
			$this->targetAttributes[$attr->nodeName] = $attr->nodeValue;
		}

		$this->collapseEditMarks($this->targetElement);
		$this->target = $this->collapseTags($this->targetElement);

		// read comments
		$comments = $xpath->evaluate('mq:comments/mq:comment', $transUnit);
		foreach ($comments as $comment)
		{
			$c = ['value' => $comment->nodeValue, 'attributes' => []];
			foreach ($comment->attributes as $attribute)
			{
				$c['attributes'][$attribute->nodeName] = $attribute->nodeValue;
			}

			$this->comments[] = $c;
		}
	}

	/**
	 * Collapse insert/delete edit marks in segment element
	 * 
	 * @param \DOMElement $element
	 */ 
	public function collapseEditMarks(\DOMElement $element)
	{
		while (true)
		{
			$marks = $element->getElementsByTagName('mrk');

			if ($marks->length === 0)
				break;
			
			foreach ($marks as $mark)
			{
				$hasInsDel = false;

				foreach ($mark->attributes as $attribute)
				{
					$parent = $mark->parentNode;

					if ($attribute->nodeName === 'mq:tctype' && $attribute->nodeValue === 'del')
					{
						$hasInsDel = true;
						$parent->removeChild($mark);

						break 2;
					}
					elseif ($attribute->nodeName === 'mq:tctype' && $attribute->nodeValue === 'ins')
					{
						$hasInsDel = true;

						foreach($mark->childNodes as $ch)
						{
							$parent->insertBefore($ch->cloneNode(true), $mark);
						}

						$parent->removeChild($mark);

						break 2;
					}
				}

				if (!$hasInsDel)
					break 2;
			}
		}
	}

	/**
	 * Expand tags placeholders to DOMElements, prepare final sourceElement/targetElement
	 * 
	 * @param \DOMDocument $dom
	 * @param string $field [source|target]
	 */ 
	public function expandTags(\DOMDocument $dom, $field)
	{
		if ($field !== 'source' && $field !== 'target')
			throw new XliffException("Tags can be expanded only in 'source' or 'target' elements");

		$newNode = $dom->createElement($field);

		// fetch available placeholders
		$placeholders = [];
		foreach ($this->tags as $tag)
		{
			$placeholders[$tag->getPlaceholder()] = $tag;
		}

		$input = $this->{$field};
		$subStr = '';
		for ($poz = 0; $poz < mb_strlen($input); $poz++)
		{
			$char = mb_substr($input, $poz, 1);
			if (isset($placeholders[$char]))
			{
				// add plain text before tag
				$newText = $dom->createTextNode($subStr);
				$newNode->appendChild($newText);

				// add tag
				$tag = $placeholders[$char];
				$newNode->appendChild($tag->write($dom));

				// clear next plain text substring
				$subStr = '';
			}
			else
			{
				// add char to new plain text substring
				$subStr .= $char;
			}
		}

		// append what's left as plain text 
		$newText = $dom->createTextNode($subStr);
		$newNode->appendChild($newText);

		$this->{$field . 'Element'} = $newNode;
	}

	/**
	 * Replace memoq inline tags in element with tags placeholders,
	 * store tags in $tags array, store plain text segment in $txt.
	 * 
	 * @param \DOMElement $element
	 * @return string
	 */ 
	public function collapseTags(\DOMElement $element)
	{
		$txt = '';
			
		foreach ($element->childNodes as $node)
		{
			if (in_array($node->nodeName, self::TAGS))
			{
				$tagAttributes = [];
				foreach ($node->attributes as $nodeAttr)
				{
					$tagAttributes[$nodeAttr->nodeName] = $nodeAttr->nodeValue;
				}

				$tag = new Tag($node->nodeName, $node->nodeValue, $tagAttributes);

				if (count($tag::PLACEHOLDERS) > count($this->tags))
				{
					if (!isset($this->tags[$tag->getHash()]))
					{
						$tag->createPlaceholder(count($this->tags));
						$this->tags[$tag->getHash()] = $tag;
					}
					$txt .= $this->tags[$tag->getHash()]->getPlaceholder();
				}
				else
				{
					$this->isValid = false;

					$newdoc = new \DOMDocument();
					$cloned = $node->cloneNode(true);

					$element_root = $newdoc->createElementNS(self::XLIFF_NS, 'root');
					$element_root->appendChild($newdoc->importNode($cloned, true));
					$newdoc->appendChild($element_root);

					$nodeString = $newdoc->saveHTML();

					// remove root element with NS declaration
					$nodeString = preg_replace('#^<root .*>#U', '', $nodeString); // U = ungreedy modifier
					$nodeString = preg_replace('#</root>$#', '', $nodeString);

					// append only inner child = cloned element
					$txt .= $nodeString;
				}
			}
			elseif ($node->nodeName === '#text')
			{
				$txt .= $node->nodeValue;
			}
		}

		return $txt;
	}

	/**
	 * Return parse segment as array
	 * 
	 * @return array
	 */ 
	public function getSegment()
	{
		return [
			'attributes' => $this->attributes,
			'tags' => $this->tags,
			'source' => $this->source,
			'sourceAttributes' => $this->sourceAttributes,
			'target' => $this->target,
			'targetAttributes' => $this->targetAttributes,
			'comments' => $this->comments,
		];
	}

	/**
	 * Set segment data from provided array
	 * 
	 * @param array $segment
	 */ 
	public function setSegment($segment)
	{
		$fields = ['source', 'sourceAttributes', 'target', 'targetAttributes', 'attributes', 'tags', 'comments'];

		foreach ($fields as $field)
		{
			if (!empty($segment[$field]))
				$this->{$field}= $segment[$field];
		}
	}

	/**
	 * Return translation unit element to be written in XLIFF file, add attributes,
	 * inject expanded inline tags.
	 * 
	 * @param \DOMDocument $dom
	 * @return \DOMElement
	 */ 
	public function write(\DOMDocument $dom)
	{
		$transUnit = $dom->createElement('trans-unit');

		// add trans-unit 
		foreach ($this->attributes as $key => $value)
		{
			$transUnit->setAttribute($key, $value);
		}

		// add source element
		$this->expandTags($dom, 'source');
		foreach ($this->sourceAttributes as $key => $value)
		{
			$this->sourceElement->setAttribute($key, $value);
		}

		// add target element
		$this->expandTags($dom, 'target');
		foreach ($this->targetAttributes as $key => $value)
		{
			$this->targetElement->setAttribute($key, $value);
		}

		$transUnit->appendChild($this->sourceElement);
		$transUnit->appendChild($this->targetElement);

		// add comments
		if (!empty($this->comments))
		{
			$commentsElement = $dom->createElement('mq:comments');
			foreach ($this->comments as $comment)
			{
				$commentElement = $dom->createElement('mq:comment', $comment['value']);
				foreach ($comment['attributes'] as $key => $value)
				{
					$commentElement->setAttribute($key, $value);
				}
				$commentsElement->appendChild($commentElement);
			}

			$transUnit->appendChild($commentsElement);
		}

		return $transUnit;
	}

	/**
	 * Return valid status
	 * 
	 * @return bool
	 */ 
	public function isValid()
	{
		return $this->isValid;
	}
}
