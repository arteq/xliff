<?php
/**
 * XLIFF file header/metadata 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */
namespace ArteQ\CSX\Xliff;

class FileHeader
{
	const XLIFF_NS = 'urn:oasis:names:tc:xliff:document:1.2';

	/**
	 * Top level <file> attributes
	 * @var array
	 */ 
	protected $attributes = [];

	/**
	 * Top level <file/header> elements without ignored
	 * @var array
	 */ 
	protected $elements = [];

	/**
	 * Header elements that are saved during read.
	 * When set to an array:
	 * - if no attributes are provided then all are saved by default
	 * - if value is set to false then element value is not saved (but its attributes and children still are)
	 * 
	 * When set to TRUE include all header elements, when set to FALSE skip all header elements.
	 * @var array|bool
	 */ 
	private $neededHeaderElements = [
		'tool' => [],
		'mq:export-path' => [], 
		'mq:docinformation' => ['attributes' => ['mq:docname'], 'value' => false],
		'mq:tagdefinition' => [],
		'mq:tagdefelement' => []
	];

	/**
	 * Read <file> element and save attributes, read <file/header> elements skipping ignored
	 * 
	 * @param \DOMDocument $dom
	 */ 
	public function read(\DOMDocument $dom)
	{
		$xpath = new \DOMXPath($dom);
		$xpath->registerNamespace('xliff', self::XLIFF_NS);

		$file = $xpath->evaluate('//xliff:file')[0];
		foreach ($file->attributes as $fileAttribute)
		{
			$this->addAttribute($fileAttribute->nodeName, $fileAttribute->nodeValue);
		}

		$headerElements = $xpath->evaluate('//xliff:file/xliff:header/*');

		foreach ($headerElements as $hdrElement)
		{
			$this->readHeaderElement($hdrElement, $this->elements);
		}
	}

	/**
	 * Create top-level <file> element with all attributes 
	 * 
	 * @param \DOMDocument $dom
	 * @return \DOMElement
	 */ 
	public function write(\DOMDocument $dom)
	{
		$file = $dom->createElement('file');
		foreach ($this->attributes as $name => $value)
		{
			$attr = $dom->createAttribute($name);
			$attr->value = $value;
			$file->appendChild($attr);
		}

		$header = $dom->createElement('header');
		foreach ($this->elements as $element)
		{
			$this->writeHeaderElement($element, $header);
		}

		$file->appendChild($header);

		return $file;
	}

	/**
	 * Add top level <file> attributes
	 * 
	 * @param string  $name
	 * @param string $value
	 */ 
	public function addAttribute($name, $value)
	{
		$this->attributes[$name] = $value;
	}

	/**
	 * Return top level <file> attributes
	 * 
	 * @return array
	 */ 
	public function getAttributes()
	{
		return $this->attributes;
	}

	/**
	 * Add top level <file/header> elements
	 * 
	 * @param string $name
	 * @param string $value
	 * @param array $attributes
	 * @param array $children
	 */ 
	public function addElement($name, $value = null, $attributes = [], $children = [])
	{
		$this->elements[] = ['name' => $name, 'value' => $value, 'attributes' => $attributes, 'children' => $children];
	}

	/**
	 * Return top level <file/header> elements
	 * 
	 * @return array
	 */ 
	public function getElements()
	{
		return $this->elements;
	}

	/**
	 * Set needed header elements that are read and stored during xliff read.
	 * By default read all (true). If set to false dont read any header elements.
	 *
	 * @param array|bool $needed
	 */
	public function setNeededHeaderElements($needed = true)
	{
		$this->neededHeaderElements = $needed;
	}

	/**
	 * Read header element, check if element is required and append to target
	 * 
	 * @param \DOMNode $node
	 * @param array $target
	 */ 
	private function readHeaderElement($node, &$target)
	{
		$nodeName = $node->nodeName;
		$nodeValue = $node->nodeValue;

		// use node value only if not a parent node with children
		if ($node->hasChildNodes())
			$nodeValue = null;

		// skip nodes that are not real elements (ex '\n' represented by #text)
		if ($node->nodeType !== \XML_ELEMENT_NODE)
			return;

		// dont store any header elements
		if ($this->neededHeaderElements === false)
		{
			return;
		}
		if (is_array($this->neededHeaderElements))
		{
			// skip elements not explicit needed
			if (!isset($this->neededHeaderElements[$nodeName]))
				return;

			// skip element value
			if (isset($this->neededHeaderElements[$nodeName]['value']))
			{
				if ($this->neededHeaderElements[$nodeName]['value'] === false)
					$nodeValue = null;
			}
		}

		$nodeAttr = [];
		foreach ($node->attributes as $attr)
		{
			// add only explicit needed
			if (isset($this->neededHeaderElements[$nodeName]['attributes']))
			{
				if (in_array($attr->nodeName, $this->neededHeaderElements[$nodeName]['attributes']))
					$nodeAttr[$attr->nodeName] = $attr->nodeValue;
			}
			else
			{
				// add all if element dont have any attr constraints
				$nodeAttr[$attr->nodeName] = $attr->nodeValue;
			}
		}

		$children = [];
		foreach ($node->childNodes as $child)
		{
			$this->readHeaderElement($child, $children);
		}

		$element = ['name' => $nodeName, 'value' => $nodeValue, 'attributes' => $nodeAttr, 'children' => $children];
		$target[] = $element;
	}

	/**
	 * Write header elements to parent node with attributes and child nodes
	 * 
	 * @param array $element
	 * @param \DOMElement $parent
	 */ 
	private function writeHeaderElement($element, &$parent)
	{
		$dom = $parent->ownerDocument;
		$node = $dom->createElement($element['name'], $element['value']);

		if (!empty($element['attributes']))
		{
			foreach ($element['attributes'] as $attribute_name => $attribute_value)
			{
				$attr = $dom->createAttribute($attribute_name);
				$attr->value = $attribute_value;
				$node->appendChild($attr);
			}
		}

		if (!empty($element['children']))
		{
			foreach ($element['children'] as $child)
			{
				$this->writeHeaderElement($child, $node);
			}
		}

		$parent->appendChild($node);
	}
}